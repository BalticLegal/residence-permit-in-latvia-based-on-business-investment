# Residence permit in Latvia based on business investment

https://www.immigration-residency.eu/residence-permit-latvia/business-investment/ - Statistics show that investing in business is a less popular way to get a residence permit compared to buying real estate. However, there are some specifics that make